# manage_admin.py 
import os 
from flask_script import Manager 
from app import create_admin_app, create_admin_app_80 
import ssl
from flask_cors import CORS

app = create_admin_app(os.getenv("TEST_APP") or "prod") 
##app = create_app()
##
CORS(app)
manager = Manager(app) 

@manager.command 
def run(): 

    app.run(host="0.0.0.0", port=5004, debug=True)
   

if __name__ == "__main__": 
    manager.run()


