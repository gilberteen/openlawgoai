 
# app/control.py
# 
import flask
from flask.helpers import make_response
import app
import os
import sys
from re import search
from typing import Awaitable
from flask import Flask, Blueprint, url_for
from flask import request, redirect, render_template, session
from flask import jsonify
from flask.wrappers import Response
from flask_wtf import form


from datetime import datetime , timedelta,date
import json
import random
import string
import ast
import pickle
import pandas as pd
from datetime import date
import openai

from .mysql import MysqldbConn


sys.path.append('.')
main_control = Blueprint('main_control',__name__)
per_page_count = 10
home_url=""


## SSL검증용
@main_control.route('/.well-known/pki-validation/gsdv.txt', methods=['GET'])
def pki_validation():
    print("pki_validation !!!")  
    
    file_basedir = os.path.dirname(__file__)
    directory = "./static/pki-validation/gsdv.txt"
    directory_full_path= os.path.join(file_basedir, directory) 

    response = make_response(open(directory_full_path).read())
    response.headers["Content-type"] = "text/plain"
    return response

###################################################################
### main
###################################################################

#@main_control.route('/user_list/<int:page_num>')
#def user_list(page_num):
@main_control.route('/main', methods=['GET'])
def main():
    print("do main()")
    userid = ""
    try:
        print("do main()")
        userid = session.get('userid', None)
        print("session , userid : {}".format(str(userid)))
        if userid == None  :
            userid = ""
    except Exception as ex:     
        print(ex)
        return jsonify({"result":"system error !"})

    
    return render_template('/home/index_lawgo.html', userid=userid )
    #return render_template('/home/chatbot.html', userid=userid )
    
@main_control.route('/logout', methods=['GET','POST'])
def logout():
    print("do logout()")

    session.pop('userid', None)
    return redirect('/')

#############################################################################
# login
@main_control.route('/login_admin', methods=['GET'])
def login_admin():
    print("login_admin()")
    return_data = ""
    try:
        print("login_admin()")
        
    except Exception as ex:     
        print(ex)
        return jsonify({"result":"system error !"})

    
    ##return render_template('/home/moodle_admin.html', return_data=return_data )
    return redirect("http://user.openedu.ai/lms/login/index.php")

@main_control.route('/login_user', methods=['GET'])
def login_user():
    print("login_user()")
    return_data = ""
    try:
        print("login_user()")
        
    except Exception as ex:     
        print(ex)
        return jsonify({"result":"system error !"})

    
    #return render_template('/home/moodle_user.html', return_data=return_data )
    return redirect("http://user.openedu.ai/lms/")


@main_control.route('/login', methods=['GET'])
def login():
    print("login()")
    return_data = ""
    try:
        print("login()")
        
    except Exception as ex:     
        print(ex)
        return jsonify({"result":"system error !"})

    
    return render_template('/login/login.html', return_data=return_data )

@main_control.route('/loginDo',methods=['GET','POST'])
def loginDo():
    print("loginDo")
    if request.method == 'POST' :
        try:
            print("loginDo")
            id = request.form['id']
            password = request.form['password']

            db_class = MysqldbConn()
            sql = f'SELECT * FROM TB_USER WHERE id = "{id}" and password = "{password}" ;'
            row = db_class.executeOne(sql)
            db_class.close_db()
            
            print("\n /loginDo , sql: {}".format(str(sql)))
            print("\n /loginDo , row: {}".format(str(row)))
            if len(str(row)) > 0 :
                result = "ok"
                data = row
                session['userid'] = row['id']
                print("\n /loginDo , session['userid']: {}".format(str(session.get('userid', None))))
            else :
                result = "fail"
                data = row

            print("\n /loginDo , result: {}".format(str(result)))
            print("\n /loginDo , data: {}".format(str(data)))

            return jsonify({"result":result,"data":data})
        except Exception as ex:     
            print(ex)
            return jsonify({"result":"fail","data":""})
    else:
        return jsonify(),204
#############################################################################

#############################################################################
# sign
@main_control.route('/signup', methods=['GET'])
def signup():
    print("signup()")
    return_data = ""
    try:
        print("signup()")
        
    except Exception as ex:     
        print(ex)
        return jsonify({"result":"system error !"})

    
    return render_template('/login/signup.html', return_data=return_data )
    #return redirect("http://user.openedu.ai/lms/login/verify_age_location.php")       


@main_control.route('/signupDo',methods=['GET','POST'])
def signupDo():
    print("signupDo")
    if request.method == 'POST' :
        try:
            print("signupDo")

            result = "fail"
            data = {}

            id = request.form['id']
            password = request.form['password']
            mobile = request.form['mobile']
            first_name = request.form['first_name']
            last_name = request.form['last_name']

            # check id
            print("signupDo1 ") 
            db_class = MysqldbConn()
            print("signupDo2 ") 
            sql = f'SELECT * FROM TB_USER WHERE id = "{id}" and password = "{password}" ;'
            row1 = db_class.executeOne(sql)
            db_class.close_db()
            print("\n sql : {}".format(str(sql)))
            print("\n row1 : {}".format(str(row1)))
            if row1 :
                print("\n id is arealy exigst !!")
                return jsonify({"result":result})

            # insert
            sql = f'INSERT INTO TB_USER (id, password , mobile, first_name, last_name) ' \
                    f'VALUES ("{id}","{password}","{mobile}","{first_name}","{last_name}") ; '
                
            print("\ntrading_new ,sql:{}".format(str(sql)))
            db_class = MysqldbConn()
            db_class.executeInsert(sql)
            db_class.commit()
            db_class.close_db()

            result = "ok"

            print("\n /signupDo , result: {}".format(str(result)))

            return jsonify({"result":result})
        except Exception as ex:     
            print(ex)
            return jsonify({"result":"fail"})
    else:
        return jsonify(),204
#############################################################################
#############################################################################
# chatbot
@main_control.route('/chatbot', methods=['GET'])
def chatbot():
    print("chatbot()")
    question = ""
   

    if request.method == 'GET' :
        try:
            question= request.args.get("question","") 
            print("chatbot() , question : {}".format(str(question)))
            return render_template('/home/chatbot.html', question=question )
        except Exception as ex:     
            print(ex)
            return render_template('/home/chatbot.html', question=question )
    else:
        return jsonify(),204
    
    

@main_control.route('/answer',methods=['GET','POST'])
def answer():
    print("answer")
    if request.method == 'POST' :
        try:
            print("answer")
            question = request.form['question']
            if(str(question) != ""):
                print("\n /answer , question: {}".format(str(question)))
                response = ""
                openai.api_key = "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"
                response = openai.Completion.create(model="text-davinci-003", prompt=question, temperature=0, max_tokens=3000)
                print(str(response))
                return_answer = "no"
                if len(str(response)) > 0 and str(response).upper() != "NO" :
                    if "choices" in response :
                        for answer in response["choices"] :
                            return_answer = str(answer["text"])
                            return_answer = return_answer.replace("\n\n","\n")
                            break
                
                print("\n /answer , return_answer: {}".format(str(return_answer)))

            return jsonify({"result":return_answer})
        except Exception as ex:     
            print(ex)
            return jsonify({"result":"no"})
    else:
        return jsonify(),204

@main_control.route('/find_id', methods=['GET','POST'])
def find_id():
    print("do find_id()")

    return render_template('/login/find_id.html')
    

@main_control.route('/getuser',methods=['GET','POST'])
def getuser():
    print("getuser")
    if request.method == 'GET' :
        try:
            print("getuser")
            id= request.args.get("id","")
            db_class = MysqldbConn()
            sql = f'SELECT * FROM TB_USER WHERE id = {id};'
            row = db_class.executeOne(sql)
            db_class.close_db()
            
            print("\n /getuser , sql: {}".format(str(sql)))
            return_val = row
            print("\n /getuser , return_val: {}".format(str(return_val)))

            return jsonify({"result":return_val})
        except Exception as ex:     
            print(ex)
            return jsonify({"result":"no"})
    else:
        return jsonify(),204

# term
@main_control.route('/terms')
def terms():
    print("terms()")
    return render_template('/user_agreement/terms_contents.html')

# privacy
@main_control.route('/privacy')
def privacy():
    print("privacy()")
    return render_template('/user_agreement/privacy.html')

