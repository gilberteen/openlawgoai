'''
https://kkamikoon.tistory.com/162
'''
import pymysql
from .settings import MYSQLDBHOST
from .settings import MYSQLDBUSER
from .settings import MYSQLDBPWD
from .settings import MYSQLDBNAME


class MysqldbConn:
    def __init__(self):
        self.db = pymysql.connect(host=MYSQLDBHOST,
                                  user=MYSQLDBUSER,
                                  password=MYSQLDBPWD,
                                  db=MYSQLDBNAME,
                                  charset='utf8'
                                  )

        self.cursor = self.db.cursor(pymysql.cursors.DictCursor)

    def executeInsert(self, query, args={}):
        self.cursor.execute(query, args)
        # get lastest AI value
        return self.cursor.lastrowid

    def executeUpdate(self, query, args={}):
        self.cursor.execute(query, args)
        # get lastest AI value
        return self.cursor.lastrowid

    def executeOne(self, query, args={}):
        self.cursor.execute(query, args)
        row = self.cursor.fetchone()
        return row

    def executeAll(self, query, args={}):
        self.cursor.execute(query, args)
        row = self.cursor.fetchall()
        return row

    def commit(self):
        self.db.commit()

    def close_db(self):
        self.db.close()
