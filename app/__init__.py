# app/__init__.py 
# 
import os
from re import T
from flask import Flask,Blueprint
from flask import request, redirect, render_template, session
from flask_wtf.csrf import CSRFProtect

from .config import config_by_name ,key
from .control import main_control
import pymysql  
pymysql.install_as_MySQLdb()

#old server
#db_url = "mysql://dba:informarkDBA1!@ec2-52-78-23-186.ap-northeast-2.compute.amazonaws.com:3306/smarthealth"

#new sever 
db_url = "mysql://openlawgoai:Openedu7474!@20.214.191.215:3306/openlawgoai"

home_url = ""

# http
ADMIN_URL = 'http://open.lawgo.ai.'
USER_URL = 'http://open.lawgo.ai.'

# https
#ADMIN_URL = 'https://open.lawgo.ai.'
#USER_URL = 'https://open.lawgo.ai.'


def create_admin_app_80(config_name="prod"):
    app = Flask(__name__) 
    
    ###################################################################
    ## before
    ###################################################################
    @app.before_request
    def redirect_path():
        return redirect(ADMIN_URL)
    
    return app


def create_admin_app(config_name="prod"):

    app = Flask(__name__) 
    ##app = Flask(__name__,host_matching=True,static_host=ADMIN_URL   ) 
    
    app.config.from_object(config_by_name[config_name]) 
    app.config['TEMPLATES_AUTO_RELOAD'] = True
    app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0

    ## db setup for sqlite
    ##basedir = os.path.abspath(os.path.dirname(__file__))
    ##dbfile = os.path.join(basedir, 'db.sqlite')
    ##app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + dbfile

    
    ## db setup for mysql
    app.config["SQLALCHEMY_DATABASE_URI"] = db_url
    app.config['SQLALCHEMY_COMMIT_ON_TEARDOWN'] = True
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False



    csrf = CSRFProtect()

    ## control app 등록
    app.register_blueprint(main_control)

    ## start thread  ( only admin)
    ####################################################################
    #threadPool.checkDeiceConnectionThread()

    ###################################################################
    ## root
    ###################################################################
    @app.route('/', methods=['GET'])
    def home():
        print("do main()")
        home_url = "/main"
        return redirect(home_url)


    ## sample router
    @app.route("/test") 
    def test(): 
        return "test" 

    return app




