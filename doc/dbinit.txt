CREATE TABLE fcuser (
	userid    VARCHAR(32) NOT NULL,
	username  VARCHAR(32),
	password  VARCHAR(128),
	bod   VARCHAR(32),
	email VARCHAR(32),
	phone_number  VARCHAR(32),
	created_at    VARCHAR(32),
	role  VARCHAR(32),
	PRIMARY KEY(userid)
) ENGINE=INNODB DEFAULT CHARSET=utf8 ;


CREATE TABLE todo (
	id  INTEGER NOT NULL, 
	fcuser_id   VARCHAR(32) NOT NULL, 
	title VARCHAR(256), 
	status INTEGER, 
	due VARCHAR(64), 
	tstamp DATETIME DEFAULT (CURRENT_TIMESTAMP), 
	PRIMARY KEY (id), 
	FOREIGN KEY(fcuser_id) REFERENCES fcuser (userid)
) ENGINE=INNODB DEFAULT CHARSET=utf8 ;
