
======================================================
깃 최초 올리기
======================================================

gitlab
gilberteen , tuesday0204

sudo git init

sudo git config  user.name “gilberteen”
sudo git config  user.email “gilberteen@gmail.com”

sudo git remote add  -t main origin https://gitlab.com/gilberteen/openedu_v1.git
sudo git remote set-url origin https://gitlab.com/gilberteen/openedu_v1.git

sudo git branch -M main

sudo git pull origin main
sudo git add .
sudo git commit -m "update chatbot"
## - GitLab Project -> Settings -> Repository -> Protected Branches -> [Expand] 버튼 클릭
sudo git push -uf origin main


[git upload]

sudo git pull origin main
sudo git add .
sudo git commit -m  'update menu'
sudo git push origin main


======================================================
[ 강제 업데이트시 ]
======================================================

cd /home/gilbert_won/openedu
sudo git fetch --all 
sudo git reset --hard origin/main
sudo git pull origin main

[
======================================================
프로세스 실행
======================================================


sudo pkill -f uwsgi -9

sudo service uwsgi restart
sudo service nginx restart

영구 실행
uwsgi /home/gilbert_won/uwsgi/uwsgi.ini &






[ 프로세스 확인 ]
cd /home/gilbert_won/drop2drop

ps -ef | grep manage_admin_443

sudo kill -9 4951


[ 관리자 서버 ]

-백그라운드 실행

클라 접속 경로 : http://api.carewellplus.co.kr/api_cc/v1/get_question_history_list?user_id=publiccare00001

cd /home/gilbert_won/drop2drop


sudo nohup python3 -u manage_admin_80.py run &
sudo nohup python3 -u manage_admin_443.py run &

sudo nohup python3 -u manage_admin_5004.py run &

sudo  python3 -u manage_admin_5004.py run

- 임시 실행

sudo python3 manage_admin_80.py run
sudo python3 manage_admin_443.py run
sudo python3 manage_admin_5004.py run

======================================================
aws linux 설치
======================================================
- aws 에서 amazon linux로 선택
- 네트워크 보안에 5000포트를 해제

- aws ternmina connect
cd <인증서 저장 경로로 이동>

aws  ec2 에 있는 경로확인후 접속함
ssh -i "smart_farm_seoul_0705b.pem" ec2-user@ec2-3-34-135-189.ap-northeast-2.compute.amazonaws.com

- create e2c
 - install git
 	sudo yum update -y
 	sudo yum install git -y
 	git version
 	
- pull git source
  mkdir smarthealth
  chmod 777 smarthealth
  
  git clone https://gitlab.com/gilberteen/informark_smart_health_server.git
  
   

======================================================
install pyton3 [linux os]
======================================================
  - install pyton3 [linux os]
    cd /opt
    sudo wget https://www.python.org/ftp/python/3.8.7/Python-3.8.7.tgz
    or
    sudo curl -O https://www.python.org/ftp/python/3.8.5/Python-3.8.5.tgz
    
    sudo tar xzf Python-3.8.7.tgz
    cd Python3.8
    sudo ./configure --enable-optimizations
    sudo yum install gcc  --> 위에 것이 오류가 있을떄만 하고 

    or
    sudo apt install gcc

    다시
    sudo ./configure --enable-optimizations
    
    sudo apt install make
    sudo make altinstall
    
    - setup path
      which python3.8  -->   여기 경로를 등록한다.  /usr/local/bin/python3.8
      
      $alias python3='/usr/local/bin/python3.8'
      $ . ~/.bashrc
      $ python3 --version
            
       $ alias python='/usr/local/bin/python3.8'
      $ . ~/.bashrc
      $ python --version
      
======================================================
페케지 설치
======================================================
오류나면.
sudo -H pip3 install --upgrade --ignore-installed pip setuptools

꼭 버전 확인할것
pip3 freeze 로 각 모듈의 버전을 확인할수 있음.

1. pip3 가 pyton3.8가 맵핑되었으면 그냥 pip3로 인스톨
pip3 --version 

으로 확인 가능하다.

sudo pip3 install -U openai
sudo pip3 install -U WTForms==2.3.3
sudo pip3 install -U Flask==1.1.4
sudo pip3 install -U Flask==1.1.2

sudo pip3 install -U flask-sqlalchemy==2.5.1
sudo pip3 install -U flask_wtf==0.15.1
sudo pip3 install  requests==2.25.1
sudo pip3 install -U flask_script==2.0.6
sudo pip3 install mysqlclient
sudo pip3 install PyMySQL==1.0.2
sudo pip3 install requests==2.25.1
sudo pip3 install pyfcm
sudo pip3 install -U flask-cors
sudo pip3 install boto3
sudo pip3 install botocore
sudo pip3 install -U Flask-Mail
sudo pip3 install -U Flask-AWSCLI
sudo pip3 install -U WTForms==2.3.3
sudo pip3 -m pip install bcrypt
sudo pip3 install -U pandas


sudo pip3 uninstall WTForms==2.3.3
sudo pip3 uninstall Flask==1.1.4
sudo pip3 uninstall flask-sqlalchemy
sudo pip3 uninstall flask_wtf
sudo pip3 uninstall  requests
sudo pip3 uninstall flask_script
sudo pip3 uninstall mysqlclient
sudo pip3 uninstall PyMySQL
sudo pip3 uninstall request
sudo pip3 uninstall pyfcm
sudo pip3 uninstall flask-cors
sudo pip3 -m pip uninstall boto3
sudo pip3 -m pip uninstall botocore
sudo python3 -m pip install Flask-Mail
sudo python3 -m pip install Flask-AWSCLI
sudo python3 -m pip install WTForms==2.3.3
sudo python3 -m pip install bcrypt


2. pip3 가 pyton3.8가 맵핑 안되었을때는 수동으로

pip3 list

sudo python3.7 -m pip3 install Flask==1.1.4
sudo python3.8 -m pip install flask-sqlalchemy
sudo python3.8 -m pip install  flask_wtf==0.15.1
sudo python3.8 -m pip install requests
sudo python3.8 -m pip install flask_script
sudo python3.8 -m pip install  Flask-Script==2.0.6
sudo python3.8 -m pip install mysqlclient
sudo python3.8 -m pip install PyMySQL
sudo python3.8 -m pip install request
sudo python3.8 -m pip install pyfcm
sudo python3.8 -m pip install -U  flask-cors
sudo python3.8 -m pip install pandas
sudo python3.8 -m pip install boto3
sudo python3.8 -m pip install botocore
sudo python3.8 -m pip install Flask-Mail
sudo python3.8 -m pip install Flask-AWSCLI
sudo python3.8 -m pip install WTForms==2.3.3
sudo python3.8 -m pip  install markupsafe==2.0.1


sudo python3 -m pip uninstall Flask==1.1.4
sudo python3 -m pip uninstall flask-sqlalchemy
pip install flask-wtf
sudo python3 -m pip uninstall flask_wtf
sudo python3 -m pip uninstall requests
sudo python3 -m pip uninstall flask_script
sudo python3 -m pip uninstall mysqlclient
sudo python3 -m pip uninstall PyMySQL
sudo python3 -m pip uninstall request
sudo python3 -m pip uninstall pyfcm
sudo python3 -m pip uninstall flask-cors
sudo python3 -m pip uninstall pandas


------------------------------------------------------------------
[mysql 설치]
------------------------------------------------------------------
0. 맥
[mysql mac 로컬 환경변수]

https://daimhada.tistory.com/121

https://gofnrk.tistory.com/15

root / tuesday0204

echo path
touch ~/.bash_profile; open ~/.bash_profile

# Setting mysql
MYSQL=/usr/ local/mysql/bin
export PATH=${PATH}:$MYSQ/

source ~/.bashrc
// or
source ~/.bash_profile
source ~/.profile
source /etc/profile

1. 리눅스 설치
https://velog.io/@sechawang/AWS-%EC%97%90-%EB%8D%B0%EC%9D%B4%ED%84%B0%EB%B2%A0%EC%9D%B4%EC%8A%A4-%EC%84%9C%EB%B2%84-%EC%85%8B%ED%8C%85%ED%95%98%EB%8A%94-%EB%B0%A9%EB%B2%95MySQL-5.7

출처: https://goddaehee.tistory.com/292 [갓대희의 작은공간]



sudo yum localinstall https://dev.mysql.com/get/mysql80-community-release-el8-3.noarch.rpm

sudo yum install mysql-community-server

2. 우분투에서 설치
https://mirae-kim.tistory.com/73
https://nickjoit.tistory.com/144

------------------------------------------------------------------
[mysql 운영]
------------------------------------------------------------------

1) mac local
xfo+Y<ytk1z2
- root 비번 확인. (아래 명령입력하면 비번 보임)
sudo grep 'temporary' /var/log/mysqld.log

- start / stop
sudo /usr/local/mysql/support-files/mysql.server start
sudo /usr/local/mysql/support-files/mysql.server stop
sudo /usr/local/mysql/support-files/mysql.server restart

- 접속
sudo mysql -u root -p
- 비번 변경
mac : root tuesday0204


2. 외부접속 허용

mysql.conf.d

▶ sudo su

▶ cd /etc/mysql/mysql.conf.d

▶ vi mysqld.cnf

3. dbbeaver 셋팅
  edit connection > connection setting > driver property 에서 allowPulicKeyRetrieval 값을  true 로변경할것
------------------------------------------------------------------

2) aws 

xfo+Y<ytk1z2

- start stop
sudo service mysql stop
sudo service mysql start

service mysqld start 
service mysqld stop 
service mysqld restart


sudo mysql
sudo mysql -u root -p

sudo mysql -u dba -p


운영서버 : root informarkDBA1!
개발서버 : root informarkDBA1!

- 비번설정
ALTER USER 'root'@'localhost' IDENTIFIED BY 'informarkDBA1!';
ALTER USER 'root'@'%' IDENTIFIED BY 'informarkDBA1!';


ALTER USER 'dba'@'localhost' IDENTIFIED BY 'informarkDBA1!';
ALTER USER 'dba'@'%' IDENTIFIED BY 'informarkDBA1!';

- db: 생성
CREATE DATABASE smarthealth default CHARACTER SET UTF8;

- 사용자 계정 생성 ,  어디서나 접속가능. ( 로컬만 하면 :  % --> localhost  로입력하면됨)
- 2개 모두 생성해야함.
create user 'dba'@'%' identified by 'informarkDBA1!';
create user 'dba'@'localhost' identified by 'informarkDBA1!';

- 비번설정
ALTER USER 'dba'@'%' IDENTIFIED BY 'informarkDBA1!';
flush privileges;

- 권한부유
grant all privileges on *.* to 'dba'@'%' with grant option;
grant all privileges on *.* to 'dba'@'localhost' with grant option;
flush privileges;

- 계정 조회
show databases;
use mysql;
select user, host  from user;

-- 데이터베이스 스타트 스탑
              우분투 명령어       	CentOS6 명령어	        CentOS7 명령어
정지	sudo service mysql stop, sudo service mysqld stop , sudo 	systemctl stop mysqld
재시작	sudo  service mysql restart, 	sudo service mysqld restart, 	sudo systemctl restart mysqld
상태확인	sudo service mysql status, 	sudo service mysqld status	, sudo systemctl status mysqld


- password reset root

sudo service mysql stop
sudo /usr/bin/mysqld_safe --skip-grant-tables &
sudo mkdir -p /var/run/mysqld
sudo chown -R mysql:mysql /var/run/mysqld

sudo mysql -u root mysql

UPDATE mysql.user SET authentication_string=PASSWORD('goeunDBA1!') WHERE user='root';

flush privileges;

# mysql uninstall

Step 1 – Uninstall MySQL Packages
sudo yum remove mysql mysql-server    #CentOS and RedHat systems 
sudo apt remove mysql mysql-server    #Ubuntu and Debian systems 
sudo dnf remove mysql mysql-server    #Fedora 22+ systems 

Step 2 – Romove MySQL Directory
sudo mv /var/lib/mysql /var/lib/mysql_old_backup
sudo mv /etc/mysql /etc/mysql_old_backup

## nohup logging

tail 명령어
파일의 뒷부분을 지정된 만큼 보여주는 명령어
기본 출력은 파일의 마지막 10줄을 보여준다
옵션
-f: 파일의 마지막 10라인을 실시간으로 계속 출력한다
예) tail -f nohup.out
-n : 원하는 수 라인 만큼 출력한다.
예) tail -n 10 nohup.out (마지막 부터 10줄을 출력한다)
예) tail -n +10 nohup.out (파일의 10번째 줄 이후부터 출력한다)
grep
문서의 특정 패턴이 들어간 라인만 출력한다, 로그 추적에 편리하다
예) tail -f nohup.out | grep "example"
마지막 부터 10줄에 example이 들어간 라인들을 출력한다
예) tail -100f nohup.out | grep "example"
마지막 부터 100줄에 example이 들어간 라인들을 출력한다.

##config 파일 분리
https://velog.io/@jisoo1170/Flask-%ED%99%98%EA%B2%BD-%EB%B6%84%EB%A6%AC%EB%A5%BC-%ED%95%B4%EB%B3%B4%EC%9E%90



======================================================
인증서 생성방법 (letsencrypt) -- 야매
======================================================

https://growingsaja.tistory.com/696


======================================================
인증서 생성방법 (정식)
======================================================
 https://dreamlog.tistory.com/375 
 
 1. 서버키 생성
 
 서버 개인키 생성
 openssl genrsa -des3 -out server.cakey.pem
 
 서버 공개키 생성
 openssl req -new -x509 -key server.cakey.pem -out root.crt
 
 서버개인키생성
 openssl genrsa -out server.key
 
서버  CSR 생성
 openssl req -new -key server.key -out server.csr

자체 서명된 인증서 생성
 openssl  x509  -req  -days 365  -in server.csr  -signkey server.cakey.pem  -out root.crt

2. 클라이언트 개인키생성


다른 방법
 https://blog.hangadac.com/2017/07/31/%ED%99%88%EC%84%9C%EB%B2%84-%EA%B5%AC%EC%B6%95%EA%B8%B0-ssl-%EC%9D%B8%EC%A6%9D%EC%84%9C-%EB%A7%8C%EB%93%A4%EA%B8%B0-%EC%97%B0%EC%8A%B5/





[ aws mail 셋업 ]

https://brunch.co.kr/@topasvga/567

https://ap-northeast-2.console.aws.amazon.com/ses/home?region=ap-northeast-2#/verified-identities


[nginx insstall]

sudo apt update
sudo apt upgrade
sudo apt autoremove

sudo apt install nginx
sudo apt-get --purge remove nginx-*

# Download nginx startup script
wget -O init-deb.sh https://www.linode.com/docs/assets/660-init-deb.sh

# Move the script to the init.d directory & make executable
sudo mv init-deb.sh /etc/init.d/nginx
sudo chmod +x /etc/init.d/nginx

# Add nginx to the system startup
sudo /usr/sbin/update-rc.d -f nginx defaults


sudo service nginx stop 
sudo service nginx start 
sudo service nginx restart
sudo service nginx reload


#uwsgi설치

sudo python3.8 -m pip  install uwsgi


======================================================
nginx , uwsgi , flask 연동
======================================================

1. nginx install 
aws : 
https://devcoops.com/install-nginx-on-aws-ec2-amazon-linux/

2. install uwsgi
#uwsgi설치

sudo python3 -m pip  install uwsgi
sudo python3 -m pip  install  uwsgi-plugin-python

pip3 list

sudo python3.8 -m pip install uwsgi

https://losskatsu.github.io/it-infra/flask-nginx-uwsgi/#flask-nginx-uwsgi-%EC%97%B0%EB%8F%99%ED%95%98%EA%B8%B0%EC%8B%A4%EC%8A%B5

https://velog.io/@twkim8548/Nginx%EC%97%90%EC%84%9C-SSL-%EC%A0%81%EC%9A%A9%ED%95%B4%EC%84%9C-Https-%EB%A1%9C-%EC%A0%91%EC%86%8D-%EB%90%98%EA%B2%8C-%ED%95%B4%EB%B3%BC%EA%B9%8C
https://medium.com/sunhyoups-story/flask-nginx-%EC%84%A4%EC%B9%98-%EB%B0%A9%EB%B2%95-258b979d2de3


https://losskatsu.github.io/it-infra/flask-nginx-uwsgi/#flask-nginx-uwsgi-%EC%97%B0%EB%8F%99%ED%95%98%EA%B8%B0%EC%8B%A4%EC%8A%B5

=====================
nginx, uwsgi 실행
=====================


sudo systemctl restart nginx

sudo systemctl restart uwsgi


sudo service uwsgi restart
sudo service nginx restart
sudo service nginx reload

임시실행
sudo uwsgi /home/gilbert_won/uwsgi/uwsgi.ini

* 위 파일에
* uwsgi /home/gilbert_won/uwsgi/uwsgi.ini  입력하면 백그라운드 실행 가능함

영구실행
uwsgi /home/gilbert_won/uwsgi/uwsgi.ini &



=====================
nginx파일 편집
=====================

* sudo vi /etc/nginx/sites-available/default 

server {
        listen 80;
	      charset utf-8;
        server_name www.openedu.ai;

        location / {
        return 307 https://www.openedu.ai;
        }
}

server {
        listen 443 ssl;
        server_name www.openedu.ai;

        ssl on;
        ssl_certificate /etc/letsencrypt/live/www.openedu.ai/fullchain.pem;
        ssl_certificate_key /etc/letsencrypt/live/www.openedu.ai/privkey.pem;
        ssl_prefer_server_ciphers on;

        add_header Strict-Transport-Security "max-age=31536000" always;
        add_header Content-Security-Policy upgrade-insecure-requests;


        location / { try_files $uri @app; }

        location @app {
        include uwsgi_params;
        uwsgi_pass unix:/home/gilbert_won/uwsgi/uwsgi.sock;
        # ex) /home/server/uwsgi.sock
    }

}


=====================
uwsgi.ini  파일작성

sudo vi  /home/gilbert_won/uwsgi/uwsgi.ini
=====================

[uwsgi]
chdir = /home/gilbert_won/openedu
uid = gilbert_won
gid = gilbert_won
enable-threads = true
chown-socket = nginx:nginx
chmod-socket = 666
socket = /home/gilbert_won/uwsgi/uwsgi.sock
module = manage_admin_5004
callable = app
daemonize = /home/gilbert_won/uwsgi/uwsgi.log




